package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;


public class SecurityExpert extends Employees {
    
    public SecurityExpert(String name, double salary) {
        if (salary < 70000.00) {
            throw new IllegalArgumentException();
        } else {
            this.name = name;
            this.salary = salary;
            this.role = "Security Expert";    
        }
        
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
