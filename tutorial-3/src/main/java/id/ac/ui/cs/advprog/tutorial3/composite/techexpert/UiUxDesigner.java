package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;


public class UiUxDesigner extends Employees {
    
    public UiUxDesigner(String name, double salary) {
        if (salary < 90000.00) {
            throw new IllegalArgumentException();
        } else {
            this.name = name;
            this.salary = salary;
            this.role = "UI/UX Designer";    
        }
        
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
