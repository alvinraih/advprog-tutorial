package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;


public class NetworkExpert extends Employees {
    
    public NetworkExpert(String name, double salary) {
        if (salary < 50000.00) {
            throw new IllegalArgumentException();
        } else {
            this.name = name;
            this.salary = salary;
            this.role = "Network Expert";    
        }
        
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
