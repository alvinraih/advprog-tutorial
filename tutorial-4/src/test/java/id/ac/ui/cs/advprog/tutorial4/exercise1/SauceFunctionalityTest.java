package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChilliSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SauceFunctionalityTest {

    private Sauce chilliSauce;
    private Sauce marinaraSauce;
    private Sauce plumTomatoSauce;

    @Before
    public void setUp() throws Exception {
        chilliSauce = new ChilliSauce();
        marinaraSauce= new MarinaraSauce();
        plumTomatoSauce= new PlumTomatoSauce();

    }

    @Test
    public void testSauceOutput(){
        assertEquals("Chilli Sauce",chilliSauce.toString());
        assertEquals("Marinara Sauce",marinaraSauce.toString());
        assertEquals("Tomato sauce with plum tomatoes",plumTomatoSauce.toString());
    }

}
