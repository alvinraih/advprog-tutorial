package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SauceTest {

    private Class<?> sauceClass;
    private Class<?> chilliSauceClass;
    private Class<?> marinaraSauceClass;
    private Class<?> plumTomatoClass;

    @Before
    public void setUp() throws Exception{
        sauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce");
        chilliSauceClass  = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChilliSauce");
        marinaraSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce");
        plumTomatoClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce");
    }

    @Test
    public void testDoughIsAPublicInterface() {
        int classModifiers = sauceClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testDoughHasToStringMethod() throws Exception {
        Method toString = sauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }

    @Test
    public void testChilliSauceIsASauceBehavior() {
        Collection<Type> classInterfaces = Arrays.asList(chilliSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce")));
    }

    @Test
    public void testChilliSauceOverrideToString() throws Exception {
        Method toString = chilliSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }

    @Test
    public void testMarinaraSauceIsASauceBehavior() {
        Collection<Type> classInterfaces = Arrays.asList(marinaraSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce")));
    }

    @Test
    public void testMarinaraSauceOverrideToString() throws Exception {
        Method toString = marinaraSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }


    @Test
    public void testPlumTomatoSauceIsASauceBehavior() {
        Collection<Type> classInterfaces = Arrays.asList(plumTomatoClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce")));
    }

    @Test
    public void testPlumTomatoSauceOverrideToString() throws Exception {
        Method toString = plumTomatoClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }
}
